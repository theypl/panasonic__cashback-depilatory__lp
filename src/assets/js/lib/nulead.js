import settings from './../lib/settings';
import {serializeArray, isJSON} from "./helpers";

const sendRequest = function (form = null, url, method, done) {
  let requestHTTP = new XMLHttpRequest();
  requestHTTP.withCredentials = false;
  
  requestHTTP.addEventListener('load', function () {
    if (requestHTTP.status >= 200 && requestHTTP.status < 300) {
      if (typeof done === "function") {
        done(requestHTTP.response, requestHTTP);
      }
    } else {
      console.log('The request failed!');
    }
  });
  
  requestHTTP.open(method, url);
  requestHTTP.send(form);
};

const nuLead = {
  set: function (GIUD = settings.GUID, $form, done) {
    let form = new FormData(),
        formDataArray = serializeArray($form),
        isGUID = false;
    
    for (let i = 0; i < formDataArray.length; i++) {
      let formDataItem = formDataArray[i];
      form.append(formDataItem.name, formDataItem.value);
      
      if (formDataItem.name === 'GUID') {
        isGUID = true;
      }
    }
    
    if (!isGUID) {
      form.append('GUID', GIUD);
    }
    
    form.append('LeadHref', window.location.href);
    form.append('LeadReferrer', document.referrer);
    form.append('LeadUA', navigator.userAgent);
    
    
    sendRequest(undefined, 'https://www.nulead.pl/ip.php', 'GET', function (dataJSON, dataHTTP) {
      
      form.append('LeadIP', dataHTTP.responseText);
      
      sendRequest(form, 'https://api.nulead.pl/set', 'POST', function (data, dataHTTP) {
        if (typeof done === "function") {
          done(JSON.parse(data), dataHTTP);
        }
      });
    });
  },
  get: function (GUID = settings.GUID, $data, done) {
    let form = new FormData(),
        obj = (isJSON($data) ? $data : JSON.parse($data));
    
    if (!('GUID' in obj)) {
      form.append('GUID', GUID);
    }
    
    Object.keys(obj).forEach(function (key) {
      form.append(key, obj[key]);
    });
    
    sendRequest(form, 'https://api.nulead.pl/get', 'POST', function (data, dataHTTP) {
      if (typeof done === "function") {
        done(JSON.parse(data), dataHTTP);
      }
    });
  },
  getJSON: function (GUID = settings.GUID, $data, done) {
    let form = new FormData(),
        obj = (isJSON($data) ? $data : JSON.parse($data));
    
    if (!('GUID' in obj)) {
      form.append('GUID', GUID);
    }
    
    Object.keys(obj).forEach(function (key) {
      form.append(key, obj[key]);
    });
    
    sendRequest(form, 'https://api.nulead.pl/get/json', 'POST', function (data, dataHTTP) {
      if (typeof done === "function") {
        done(JSON.parse(data), dataHTTP);
      }
    });
  },
  count: function (GUID = settings.GUID, $data, done) {
    let form = new FormData(),
        obj = (isJSON($data) ? $data : JSON.parse($data));
    
    if (!('GUID' in obj)) {
      form.append('GUID', GUID);
    }
    
    Object.keys(obj).forEach(function (key) {
      form.append(key, obj[key]);
    });
    
    sendRequest(form, 'https://api.nulead.pl/count', 'POST', function (data, dataHTTP) {
      if (typeof done === "function") {
        done(JSON.parse(data), dataHTTP);
      }
    });
  },
  update: function (GUID = settings.GUID, $lid, $data, done) {
    let form = new FormData(),
        obj = (isJSON($data) ? $data : JSON.parse($data));
    
    if (!('GUID' in obj)) {
      form.append('GUID', GUID);
    }
    
    form.append('LID', $lid);
    
    Object.keys(obj).forEach(function (key) {
      form.append(key, obj[key]);
    });
    
    sendRequest(form, 'https://api.nulead.pl/get', 'POST', function (data, dataHTTP) {
      if (typeof done === "function") {
        done(JSON.parse(data), dataHTTP);
      }
    });
  },
};

export default nuLead;
