const translations = {
  pl: {
    validation: {
      messages: {
        missingValue: {
          checkbox: 'To pole jest wymagane.',
          radio: 'Wybierz jedną z opcji',
          select: 'Wybierz jedną z opcji',
          'select-multiple': 'Wybierz przynajmniej jedną opcję',
          default: 'To pole jest wymagane.'
        },
        patternMismatch: {
          email: 'Wprowadź poprawny adres e-mail.',
          url: 'Wprowadź poprawny URL.',
          number: 'Wprowadź poprawny numer.',
          color: 'Wprowadź kolor według formatu: #rrggbb',
          date: 'Użyj formatu YYYY-MM-DD',
          time: 'Please use the 24-hour time format. Ex. 23:00',
          month: 'Please use the YYYY-MM format',
          default: 'Please match the requested format.'
        },
        outOfRange: {
          over: 'Please select a value that is no more than {max}.',
          under: 'Please select a value that is no less than {min}.'
        },
        wrongLength: {
          over: 'Please shorten this text to no more than {maxLength} characters. You are currently using {length} characters.',
          under: 'Treść nie może być krótsza niż {minLength} znaków. Wprowadzono {length} znaków.'
        }
      },
    }
  },
  en: {
    validation: {
      messages: {
        missingValue: {
          checkbox: 'This field is required.',
          radio: 'Please select a value.',
          select: 'Please select a value.',
          'select-multiple': 'Please select at least one value.',
          default: 'Please fill out this field.'
        },
        patternMismatch: {
          email: 'Please enter a valid email address.',
          url: 'Please enter a URL.',
          number: 'Please enter a number',
          color: 'Please match the following format: #rrggbb',
          date: 'Please use the YYYY-MM-DD format',
          time: 'Please use the 24-hour time format. Ex. 23:00',
          month: 'Please use the YYYY-MM format',
          default: 'Please match the requested format.'
        },
        outOfRange: {
          over: 'Please select a value that is no more than {max}.',
          under: 'Please select a value that is no less than {min}.'
        },
        wrongLength: {
          over: 'Please shorten this text to no more than {maxLength} characters. You are currently using {length} characters.',
          under: 'Please lengthen this text to {minLength} characters or more. You are currently using {length} characters.'
        }
      },
    }
  },
  hu: {
    validation: {
      messages: {
        missingValue: {
          checkbox: 'Ez a mező kötelező.',
          radio: 'Please select a value.',
          select: 'Please select a value.',
          'select-multiple': 'Please select at least one value.',
          default: 'Ez a mező kötelező.'
        },
        patternMismatch: {
          email: 'Kérjük valós e-mail címet adjon meg!',
          url: 'Please enter a URL.',
          number: 'Please enter a number',
          color: 'Please match the following format: #rrggbb',
          date: 'Kérjük, használja az ÉÉÉÉ-HH-NN formátumot',
          time: 'Please use the 24-hour time format. Ex. 23:00',
          month: 'Please use the YYYY-MM format',
          default: 'Kérjük, egyeztesse a kért formátumot.'
        },
        outOfRange: {
          over: 'Válasszon egy értéket, amely nem haladja meg a (z) {max} értéket.',
          under: 'Válasszon legalább {min} értéket.'
        },
        wrongLength: {
          over: 'Please shorten this text to no more than {maxLength} characters. You are currently using {length} characters.',
          under: 'Please lengthen this text to {minLength} characters or more. You are currently using {length} characters.'
        }
      },
    }
  },
  cz: {
    validation: {
      messages: {
        missingValue: {
          checkbox: 'Toto pole je povinné.',
          radio: 'Vyberte hodnotu.',
          select: 'Vyberte hodnotu.',
          'select-multiple': 'Vyberte alespoň jednu hodnotu.',
          default: 'Prosím, vyplňte toto pole.'
        },
        patternMismatch: {
          email: 'Prosím zadejte platnou emailovou adresu.',
          url: 'Zadejte prosím adresu URL.',
          number: 'Zadejte číslo',
          color: 'Zadejte prosím následující formát: #rrggbb',
          date: 'Použijte prosím formát RRRR-MM-DD',
          time: 'Použijte prosím 24hodinový formát času. Př. 23:00',
          month: 'Použijte prosím formát RRRR-MM',
          default: 'Zadejte požadovaný formát.'
        },
        outOfRange: {
          over: 'Vyberte hodnotu, která není vyšší než {max}.',
          under: 'Vyberte hodnotu, která není menší než {min}.'
        },
        wrongLength: {
          over: 'Zkraťte prosím tento text na více než {maxLength} znaků. Momentálně používáte znaky {length}.',
          under: 'Prodlužte prosím tento text na {minLength} znaků nebo více. Momentálně používáte znaky {length}.'
        }
      },
    }
  }
};

let websiteLang = document.getElementsByTagName('html')[0].getAttribute('lang');

function i18n(lang = websiteLang) {
  return translations[lang];
}

export default i18n;
