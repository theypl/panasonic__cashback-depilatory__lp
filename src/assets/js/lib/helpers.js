const serializeArray = function (form) {
  let serialized = [];
  
  for (let i = 0; i < form.elements.length; i++) {
    
    let field = form.elements[i];
    
    console.log(field);
    
    
    if (!field.name || field.disabled || field.type === 'reset' || field.type === 'submit' || field.type === 'button') continue;
    
    if (field.type === 'select-multiple') {
      for (let n = 0; n < field.options.length; n++) {
        if (!field.options[n].selected) continue;
        serialized.push({
          name: field.name,
          value: field.options[n].value
        });
      }
    } else if (field.type === 'file') {
      for (let f = 0; f < field.files.length; f++) {
        serialized.push({
          name: field.name + f,
          value: field.files[f]
        });
      }
    } else if ((field.type !== 'checkbox' && field.type !== 'radio') || field.checked) {
      serialized.push({
        name: field.name,
        value: field.value
      });
    }
  }
  
  return serialized;
};

const isJSON = function (data) {
  if (typeof data !== 'string') {
    data = JSON.stringify(data);
  }
  
  try {
    JSON.parse(data);
    return true;
  } catch (e) {
    return false;
  }
};

export {serializeArray, isJSON};
