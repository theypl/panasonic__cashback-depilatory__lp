import {scrollTo} from 'scroll-js';

function scrollIt(position, duration = 700, easing = 'ease-in-out', toScroll = document.body, callback) {
  scrollTo(toScroll, {
    top: position,
    duration: duration,
    easing: easing
  }).then(
      () => callback ? callback : undefined
  );
}

function scrollToElem(trigger = '[data-scroll]', attribute = 'data-scroll', duration = 700, offset = 0, easing = 'ease-in-out', callback) {
  
  let eventElement = document.querySelectorAll(trigger);
  
  if (typeof (eventElement) !== 'undefined' && eventElement !== null) {
    for (let i = 0; i < eventElement.length; i++) {
      eventElement[i].addEventListener('click', function (event) {
        event.preventDefault();
        
        scrollIt(
            document.querySelector(this.getAttribute(attribute)).offsetTop + offset,
            duration,
            easing,
            document.body,
            () => callback ? callback : undefined
        );
      });
    }
  }
}

export {scrollIt, scrollToElem};
