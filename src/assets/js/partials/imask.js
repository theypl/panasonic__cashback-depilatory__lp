import IMask from 'imask'

function imask() {

    const zipcodeMask = IMask(
        document.getElementById('zipcode'),
        {
            mask: '00-000'
        });

    const phoneMask = IMask(
        document.getElementById('phone'),
        {
            mask: [
                {
                    mask: '000 000 000'
                },
                {
                    mask: '00 00 00 000'
                },
                {
                    mask: '+00 00 00 000'
                },
                {
                    mask: '(00) 00 00 000'
                }
            ]
        });

    const transferZipcodeMask = IMask(
        document.getElementById('transfer-zipcode'),
        {
            mask: '00-000'
        });

    const transferBankNumberMask = IMask(
        document.getElementById('transfer-bank-number'),
        {
            mask: '00-0000-0000-0000-0000-0000-0000'
        });

}

export default imask;
