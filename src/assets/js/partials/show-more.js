function showMore() {
    const checkboxes = document.getElementsByClassName('pan-form__checkbox-cont');

    for (let i = 0; i < checkboxes.length; i++) {

        let  el = checkboxes[i];
        let btn = el.querySelector('button');
        btn.addEventListener('click', function (e) {
            e.preventDefault();

            if (el.classList.contains('more')) {
                el.classList.remove('more');
                btn.innerText='CZYTAJ WIĘCEJ'
            } else {
                el.classList.add('more');
                btn.innerText='CZYTAJ MNIEJ'
            }
        })
    }
}

export default showMore;
