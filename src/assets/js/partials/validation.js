import Bouncer from 'formbouncerjs';
import nuLead from './../lib/nulead';
import settings from './../lib/settings';
import i18n from '../lib/translations';

// read more https://github.com/cferdinandi/bouncer

function validation() {
    // initialize Bouncer in variable, to use with events like 'validate(), validateAll() and destroy()
    const validate = new Bouncer('.pan-form', {

        // Custom validations
        customValidations: {
            fileSize: function (field) {
                let maxSize = field.getAttribute('data-bouncer-maxsize');
                if (Boolean(maxSize)) {
                    if (Boolean(field.files[1])) {
                        if (field.files[0].size > maxSize || field.files[1].size > maxSize) {
                            return true;
                        } else {
                            return false;
                        }
                    } else if (Boolean(field.files[0])) {
                        if (field.files[0].size > maxSize) {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            },
            fileType: function (field) {
                if (Boolean(field.accept)) {
                    const allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif|\.pdf|\.doc|\.docx)$/i;
                    const disallowedNames = ['.php'];
                    let filePath = field.value;
                    console.log(filePath);

                    const hiddenExtensionCheck = (str) => {
                        let passed = true;

                        disallowedNames.forEach((word) => {
                            str.includes(word) ? (passed = false) : '';
                        });

                        return passed;
                    };

                    if (!allowedExtensions.exec(filePath) || !hiddenExtensionCheck(filePath)) {
                        console.log('fileType error');
                        return true;
                    } else {
                        console.log('fileType notError');
                        return false;
                    }
                } else {
                    return false;
                }
            },
        },

        // Classes & IDs
        fieldClass: 'error', // Applied to fields with errors
        errorClass: 'error-message', // Applied to the error message for invalid fields
        fieldPrefix: 'bouncer-field_', // If a field doesn't have a name or ID, one is generated with this prefix
        errorPrefix: 'bouncer-error_', // Prefix used for error message IDs

        // Patterns
        // Validation patterns for specific input types
        patterns: {
            email: /^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*(\.\w{2,})+$/,
            url: /^(?:(?:https?|HTTPS?|ftp|FTP):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-zA-Z\u00a1-\uffff0-9]-*)*[a-zA-Z\u00a1-\uffff0-9]+)(?:\.(?:[a-zA-Z\u00a1-\uffff0-9]-*)*[a-zA-Z\u00a1-\uffff0-9]+)*(?:\.(?:[a-zA-Z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/,
            number: /[-+]?[0-9]*[.,]?[0-9]+/,
            color: /^#?([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$/,
            date: /(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))/,
            time: /(0[0-9]|1[0-9]|2[0-3])(:[0-5][0-9])/,
            month: /(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2]))/
        },

        // Message Settings
        messageAfterField: true, // If true, displays error message below field. If false, displays it above.
        messageCustom: 'data-bouncer-message', // The data attribute to use for customer error messages
        messageTarget: 'data-bouncer-target', // The data attribute to pass in a custom selector for the field error location

        // Error messages by error type
        messages: {
            missingValue: {
                checkbox: i18n().validation.messages.missingValue.checkbox,
                radio: i18n().validation.messages.missingValue.radio,
                select: i18n().validation.messages.missingValue.checkbox,
                'select-multiple': i18n().validation.messages.missingValue['select-multiple'],
                default: i18n().validation.messages.missingValue.default
            },
            patternMismatch: {
                email: i18n().validation.messages.patternMismatch.email,
                url: i18n().validation.messages.patternMismatch.url,
                number: i18n().validation.messages.patternMismatch.number,
                color: i18n().validation.messages.patternMismatch.color,
                date: i18n().validation.messages.patternMismatch.date,
                time: i18n().validation.messages.patternMismatch.time,
                month: i18n().validation.messages.patternMismatch.month,
                default: i18n().validation.messages.patternMismatch.default
            },
            outOfRange: {
                over: i18n().validation.messages.outOfRange.over,
                under: i18n().validation.messages.outOfRange.under
            },
            wrongLength: {
                over: i18n().validation.messages.wrongLength.over,
                under: i18n().validation.messages.wrongLength.under
            },
            fileSize: 'max 5 MB',
            fileType: '*.jpg, *.jpeg, *.png, *.gif, *.pdf, *.doc, *.docx',
        },

        // Form Submission
        disableSubmit: true, // If true, native form submission is suppressed even when form validates

        // Custom Events
        emitEvents: true // If true, emits custom events

    });

    // Detect show error events
    document.addEventListener('bouncerShowError', function (event) {

        // The field with the error
        const field = event;

        // console.log(event);

    }, false);

    // Detect a successful form validation
    document.addEventListener('bouncerFormValid', function (event) {

        // The successfully validated form
        const form = event.target;

        let GUID;
        let lang = document.documentElement.lang;

        switch (lang) {
            case 'hu':
                GUID = settings.GUIDhu;
                break;
            case 'cz':
                GUID = settings.GUIDcz;
                break;
            default:
                GUID = settings.GUID;
        }

        // console.log(form);
        nuLead.set(GUID, form, function (dataJSON, dataHTTP) {
            // callback
            // console.log(dataJSON, dataHTTP);

            if (dataJSON.status == 'error') {
                $('#errorModal').foundation('open');
            } else {
                $('#succesModal').foundation('open');
                form.reset();
                window.scrollTo(0, 0);
                //GTM event
                window.dataLayer = window.dataLayer || [];
                window.dataLayer.push({
                    'event': 'form_submit-event'
                });
            }
        })
        // If `disableSubmit` is true, you might use this to submit the form with Ajax

    }, false);
}

export default validation;
