function matInput() {
    const inputsList = document.getElementsByClassName('mat__input');

    for (let i = 0; i < inputsList.length; i++) {
        let input = inputsList[i];
        let box = input.parentElement;

        if (input.value !== "") {
            box.classList.add('active')
        } else {
            box.classList.remove('active')
        }

        input.addEventListener("change", function () {
                if (input.value !== "") {
                box.classList.add('active')
            } else {
                box.classList.remove('active')
            }
        });

        input.addEventListener("focusout", function () {
            if (input.value !== "") {
                box.classList.add('active')
            } else {
                box.classList.remove('active')
            }
        });

        input.addEventListener("onfocus", function () {
            if (input.value !== "") {
                box.classList.add('active')
            } else {
                box.classList.remove('active')
            }
        })
    }

    // reset form
    const form = document.querySelector('form');

    form.addEventListener('reset', function () {
        for (let i = 0; i < inputsList.length; i++) {
            let input = inputsList[i];
            let box = input.parentElement;
            box.classList.remove('active')
        }
    });


    // set today date after date input active
    Date.prototype.toDateInputValue = (function() {
        let local = new Date(this);
        local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
        return local.toJSON().slice(0,10);
    });

    document.querySelector('input[type="date"]').value = new Date().toDateInputValue();
}

export default matInput;


