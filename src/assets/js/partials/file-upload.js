function fileUpload() {
    const inputs = document.querySelectorAll('#file_receipt');
    Array.prototype.forEach.call(inputs, function (input) {
        const container = input.parentElement,
            labelVal = container.querySelector('label').querySelector('span').innerHTML,
            fileInput = container.querySelector('input');


        fileInput.addEventListener('change', function (e) {
            let fileName = '';
            if (this.files && this.files.length > 1) {
                fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
            } else
                fileName = e.target.value.split('\\').pop();

            if (fileName)
                container.querySelector('label').querySelector('span').innerHTML = fileName;
            else
                container.querySelector('label').querySelector('span').innerHTML = labelVal;
        });

        let form2 = input.parentElement;

        (function searchForm() {
            if (form2.nodeName == 'FORM') {
                return form2
            } else {
                form2 = form2.parentElement;
                searchForm()
            }
        })();

        form2.addEventListener('reset', function () {
            container.querySelector('label').querySelector('span').innerHTML = labelVal;
        })
    });
}

export default fileUpload;
