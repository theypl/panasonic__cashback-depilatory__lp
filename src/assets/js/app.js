import $ from 'jquery';
import 'what-input';
import './lib/foundation-pieces';

// import validation partial
import validation from './partials/validation';
// import scroll partial
// import {scrollToElem} from "./lib/scrollTo";

// sample partial
import samplePartialFunction from './partials/sample-partial';
import matInput from './partials/materialize-input';
import showMore from './partials/show-more';
import fileUpload from './partials/file-upload';
import imask from './partials/imask';

const app = {
  init: function () {
    $(document).foundation();

    // call validation function
    validation();

    // docs in README.md
    // scrollToElem('[data-scroll]', 'href');

    matInput();
    showMore();
    fileUpload();

    let lang = document.documentElement.lang;
    lang == 'pl' ? imask() : '';
  }
};

document.addEventListener('DOMContentLoaded', function () {
  app.init();
});

