# Foundation Starter Template

## Installation

To use this template, your computer needs:

- [NodeJS](https://nodejs.org/en/) (0.12 or greater)
- [Git](https://git-scm.com/)

## SCSS

`src/assets/scss/app.scss` - foundation components import
`src/assets/scss/_settings.scss` - all foundation settings

## Config

Whole configuration is in `config.yml` file 

Template uses **Babel** and **Webpack** to compile Javascript.

## Translations

Use {${ filename.your-translations }$} syntax.

The default directory (config.yml --> TRANSLATION --> default) may be empty, but it must exist in /src/local

Every translation may have unlimited number of files.

## lib/Scroll

`scrollToElem()` - adds an eventListener 'click'. 

Options:
- **triggerElement** = '[data-scroll]' 
- **attribute** = 'data-scroll'
- **duration** = 500 
- **offset** = 0
- **easing** = 'ease-in-out'
- **callback** = function

Example of use:

```
scrollToElem('[data-scroll]', 'href');
```
---
`scrollIt()` - scroll to element or position.

Example of use:

```
scrollIt(
    document.querySelector('#section').offsetTop, // number (offsetTop)
    500, // duration
    'ease-in-out', // easing
    document.body, // element to scroll, default document.body
    () => console.log(`Just finished scrolling to ${window.pageYOffset}px`) // callback
);

```

**EASINGS:**
- linear
- ease-in
- ease-out
- ease-in-out _//default_
