const gulp = require('gulp');
const browserSync = require('browser-sync');
const $ = require('gulp-load-plugins')();
const autoprefixer = require('autoprefixer');
const rimraf = require('rimraf');
const yaml = require('js-yaml');
const named = require('vinyl-named');
const fs = require('fs');
const yargs = require('yargs');
const tinypng = require('gulp-tinypng-extended');
const svgmin = require('gulp-svgmin');
const gulpif = require('gulp-if');
const sourcemaps = require('gulp-sourcemaps');
const i18n = require('gulp-html-i18n');
const replace = require('gulp-replace');
const rev = require('gulp-rev');
const revRewrite = require('gulp-rev-rewrite');
const revDelete = require('gulp-rev-delete-original');

const webpackStream = require('webpack-stream');
const webpack2 = require('webpack');

// Load settings from settings.yml
const {PATHS, TINYPNGAPI, TRANSLATION} = loadConfig();

// Check for --production flag
const PRODUCTION = !!(yargs.argv.production);

const webpackMode = PRODUCTION ? 'production' : 'development';

let webpackConfig = {
  mode: (webpackMode),
  module: {
    rules: [
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ["@babel/preset-env"],
            compact: false
          }
        }
      }
    ]
  },
  plugins: [
    new webpack2.ProvidePlugin({
      jQuery: 'jquery',
      $: 'jquery'
    })
  ],
  devtool: false,
  performance: {
    hints: false,
    maxEntrypointSize: 512000,
    maxAssetSize: 512000
  },
  optimization: {
    minimize: PRODUCTION
  }
};


function loadConfig() {
  let ymlFile = fs.readFileSync('config.yml', 'utf8');
  return yaml.load(ymlFile);
}

function copy() {
  return gulp.src(PATHS.assets)
  .pipe(gulp.dest(PATHS.dist + '/assets'));
}

function clean(done) {
  rimraf(PATHS.dist, done);
}

function html() {
  return gulp.src('src/*.html')
  .pipe(i18n({
    langDir: './src/locale',
    createLangDirs: true,
    trace: true,
    defaultLang: TRANSLATION.default,
    delimiters: ['{${', '}$}']
  }))
  .pipe(gulp.dest(PATHS.dist))
  .on('end', function () {
    if (!PRODUCTION) {
      assetsPathReplace();
    }
  })
}

function images() {
  if (PRODUCTION && TINYPNGAPI) {
    return gulp.src(['src/assets/img/**/*.jpg', 'src/assets/img/**/*.png', 'src/assets/img/**/*.jpeg'])
    .pipe(tinypng({
      key: TINYPNGAPI,
      log: true
    })).on('end', function () {
      return gulp.src(['src/assets/img/**/*.ico'])
      .pipe(gulp.dest(PATHS.dist + '/assets/img'));
    })
    .pipe(gulp.dest(PATHS.dist + '/assets/img'));
  } else {
    return gulp.src(['src/assets/img/**/*.jpg', 'src/assets/img/**/*.png', 'src/assets/img/**/*.jpeg', 'src/assets/img/**/*.ico'])
    .pipe(gulp.dest(PATHS.dist + '/assets/img'));
  }
}

function svg() {
  return gulp.src('src/assets/img/**/*.svg')
  .pipe(gulpif(PRODUCTION, svgmin()))
  .pipe(gulp.dest(PATHS.dist + '/assets/img'));
}


function sass() {
  return gulp.src('src/assets/scss/app.scss')
  .pipe(gulpif(!PRODUCTION, sourcemaps.init()))
  .pipe($.sass({
    includePaths: PATHS.sass,
    outputStyle: 'compressed' // if css compressed **file size**
  })
  .on('error', $.sass.logError))
  .pipe(gulpif(PRODUCTION, $.postcss([
    autoprefixer()
  ]))
  .pipe(gulpif(!PRODUCTION, sourcemaps.write())))
  .pipe(gulp.dest(PATHS.dist + '/assets/css'))
  .pipe(browserSync.reload({stream: true}));
}


function scripts() {
  return gulp.src(PATHS.entries)
  .pipe(named())
  .pipe(webpackStream(webpackConfig, webpack2))
  .pipe(gulpif(PRODUCTION, $.uglify()
  .on('error', e => {
        console.log(e);
      }
  )))
  .pipe(gulp.dest(PATHS.dist + '/assets/js'));
}


function assetsPathReplace() {
  return gulp.src(PATHS.dist + '/*/**/*.html')
  .pipe(replace('assets/', '../assets/'))
  .pipe(gulp.dest(PATHS.dist))
}

function revisionAssets() {
  return gulp.src('dist/**/*.{css,js}')
  .pipe(rev())
  .pipe(revDelete()) // Remove the unrevved files
  .pipe(gulp.dest('dist'))
  .pipe(rev.manifest())
  .pipe(gulp.dest('dist'));
}

function revision(done) {
  const manifest = gulp.src(PATHS.dist + '/rev-manifest.json');
  
  return gulp.src(PATHS.dist + '/**/*.html')
  .pipe(revRewrite({manifest}))
  .pipe(gulp.dest(PATHS.dist))
  .on('end', function () {
    rimraf(PATHS.dist + '/rev-manifest.json', done);
  });
}

function server(done) {
  browserSync.init({
    server: PATHS.dist
  }, done);
}


// Watch for changes to static assets, HTML, Sass, and JavaScript
function watch() {
  gulp.watch(PATHS.assets, copy);
  gulp.watch('src/assets/scss/**/*.scss').on('all', sass);
  gulp.watch('src/assets/js/**/*.js').on('all', gulp.series(scripts, browserSync.reload));
  gulp.watch('src/**/*.html').on('all', gulp.series(html, browserSync.reload));
  gulp.watch('src/assets/img/**/*').on('all', gulp.series(images, svg, browserSync.reload));
}

gulp.task('buildProduction', gulp.series(clean, gulp.parallel(html, scripts, images, svg, copy), sass, revisionAssets, assetsPathReplace, revision));
gulp.task('buildDev', gulp.series(clean, gulp.parallel(html, scripts, images, svg, copy), sass));
gulp.task('default', gulp.series('buildDev', server, watch));
